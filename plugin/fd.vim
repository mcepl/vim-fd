function! FD()
  let ofile = expand('%:p')
  if getftype(ofile) == "link"
      let file = resolve(ofile)
      let dir  = fnamemodify(file, ":p:h")
      execute 'cd' fnameescape(dir)
      write!
      bwipeout!
      execute 'edit!' fnameescape(file)
  endif
endfunction

map E :call FD()<CR>

Vim command FD()
----------------

For heavy users of symlinks, this command will switch to “the 
true location” of the file. I.e., when I have symlink 
``~/TODO.txt`` which points to 
``~/archive/2008/work/technical/TODO.txt`` and there is 
a mapping::

    map E :call FD()<CR>

Then after pressing ``E`` command, you are editing the symlinked 
file and (if ``autochdir`` option is on) current directory is 
``~/archive/2008/work/technical/``.
